<?php
$var = '';

// this will evaluate to TRUE so the text willl be printed.
if (isset($var)){
    echo "This var is set so I will print.";

}
// In the next example we will use var_ dump to output
//the return value of isset().
$a = "test";
$b = "anothertest";
var_dump(isset($a)); //true
var_dump(isset($a, $b)); //true
unset ($a);
var_dump(isset($a)); //false
var_dump(isset($a, $b)); //false
$foo = NULL;
var_dump(isset($foo)); // false
?>